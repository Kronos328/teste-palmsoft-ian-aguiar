package br.com.testpalmsoft.gamecrud.control;

import java.util.Optional;
import java.util.stream.LongStream;

import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.testpalmsoft.gamecrud.domain.Game;
import br.com.testpalmsoft.gamecrud.domain.GameRepository;

@Controller
public class GameController {

	private GameRepository gameRepo;

	public GameController(GameRepository repo) {
		this.gameRepo = repo;
	}

	@GetMapping("/")
	public String index() {
		return "redirect:/games";
	}

	@GetMapping("/games")
	public String games(Model model, Optional<Integer> pageNumber, Optional<Integer> pageSize) {

		PageRequest pageReq = setupPagination(pageNumber, pageSize, model);

		model.addAttribute("gameList", gameRepo.findAll(pageReq));

		return "games/index";
	}

	@GetMapping("/game/search")
	public String gameSearch(Model model, String idSearch) {
		Optional<Game> foundGameOpt = gameRepo.findById(Long.parseLong(idSearch));

		if (foundGameOpt.isEmpty()) {
			return "error/game_not_found";
		}

		model.addAttribute("game", foundGameOpt.get());
		return "games/single_game";
	}

	@GetMapping("game/{id}")
	public String getGame(@PathVariable("id") long id, Model model) {

		Optional<Game> gameOpt = gameRepo.findById(id);
		if (gameOpt.isEmpty()) {
			return "error/game_not_found";
		}

		model.addAttribute("game", gameOpt.get());
		return "games/single_game";
	}

	@PostMapping("game/saveAll")
	public String saveAllGame(@Valid @ModelAttribute("game") Game game, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "games/change_all_game";
		}

		gameRepo.save(game);
		return "redirect:/games";
	}

	@PostMapping("game/saveName")
	public String saveNameGame(@Valid @ModelAttribute("game") Game game, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "games/change_name_game";
		}

		gameRepo.save(game);
		return "redirect:/games";
	}

	@GetMapping("game/{id}/changeAll")
	public String changeAllGame(@PathVariable("id") long id, Model model) {

		Optional<Game> gameOpt = gameRepo.findById(id);
		if (gameOpt.isEmpty()) {
			return "error/game_not_found";
		}

		model.addAttribute("game", gameOpt.get());

		return "games/change_all_game";
	}

	@GetMapping("game/{id}/changeName")
	public String changeNameGame(@PathVariable("id") long id, Model model) {

		Optional<Game> gameOpt = gameRepo.findById(id);
		if (gameOpt.isEmpty()) {
			return "error/game_not_found";
		}

		model.addAttribute("game", gameOpt.get());

		return "games/change_name_game";
	}

	@GetMapping("game/{id}/delete")
	public String deleteGame(@PathVariable("id") long id) {

		Optional<Game> gameOpt = gameRepo.findById(id);
		if (gameOpt.isEmpty()) {
			return "error/game_not_found";
		} else if (gameOpt.get().getPrice() == 10.00) {
			return "error/cannot_delete_game";
		}

		gameRepo.delete(gameOpt.get());
		return "redirect:/games";
	}

	@GetMapping("game/new")
	public String addNewGame(Model model) {
		Game newGame = new Game();

		model.addAttribute("game", newGame);

		return "games/new_game";
	}

	private PageRequest setupPagination(Optional<Integer> pageNumber, Optional<Integer> pageSize, Model model) {
		int currentPage = pageNumber.orElse(1) - 1;
		int itemsPerPage = pageSize.orElse(5);
		long totalItems = gameRepo.count();
		long totalPageCount = (totalItems / itemsPerPage);

		if (totalItems % itemsPerPage != 0) {
			totalPageCount++;
		}

		LongStream totalPages = LongStream.range(1, totalPageCount + 1);
		model.addAttribute("totalPages", totalPages.iterator());		

		return PageRequest.of(currentPage, itemsPerPage, Sort.by("id"));
	}

}
