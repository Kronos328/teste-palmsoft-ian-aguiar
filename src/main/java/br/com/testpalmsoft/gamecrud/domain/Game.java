package br.com.testpalmsoft.gamecrud.domain;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Game {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotBlank(message = "O nome do jogo não pode ser vazio")
	private String name;
	
	@NotBlank(message = "O nome da desenvolvedora não pode ser vazio")
	private String developerName;
	
	@DecimalMin(value = "0.00", message = "O preço de um jogo não pode ser menor do que 0")
	private double price;
	
	@NotNull(message = "Data de criação não pode estar vazia")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate createdAt;
	
	@NotNull(message = "Data de edição não pode estar vazia")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate editedAt;
	
	// TODO Image
	
	public Game() {}
	
	public Game(String name, String developerName, double price) {
		this.name = name;
		this.developerName = developerName;
		this.price = price;
		
		this.createdAt = LocalDate.now();
		this.editedAt = LocalDate.now();		
	}	

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		return id == other.id;
	}
	
	
	public long getId() {
		return id;
	}
	
	
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getDeveloperName() {
		return developerName;
	}
	
	
	public void setDeveloperName(String developerName) {
		this.developerName = developerName;
	}
	
	
	public double getPrice() {
		return price;
	}
	
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	
	public LocalDate getCreatedAt() {
		return createdAt;
	}
	
	
	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}
	
	
	public LocalDate getEditedAt() {
		return editedAt;
	}
	
	
	public void setEditedAt(LocalDate editedAt) {
		this.editedAt = editedAt;
	}
	

}
