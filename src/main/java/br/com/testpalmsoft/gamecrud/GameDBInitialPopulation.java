package br.com.testpalmsoft.gamecrud;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import br.com.testpalmsoft.gamecrud.domain.Game;
import br.com.testpalmsoft.gamecrud.domain.GameRepository;
import io.github.cdimascio.dotenv.Dotenv;

@Component
@Transactional
public class GameDBInitialPopulation implements CommandLineRunner{
	
	@Autowired
	private GameRepository gameRepo;
	
	@Override
	public void run(String... args) throws Exception {
		Dotenv dotenv = Dotenv.load();
		
		boolean populateDb = Boolean.parseBoolean(dotenv.get("POPULATE_DB"));
		boolean isDbEmpty = gameRepo.findAll().isEmpty();
		
		if (!isDbEmpty || (isDbEmpty && !populateDb)){
			return;
		}
		
		Game game1 = new Game("Dark Souls", "From Software", 39.99);
		Game game2 = new Game("Slay the Spire", "Mega Crit", 29.99);
		Game game3 = new Game("Super Mario Odissey", "Nintendo", 79.99);
		Game game4 = new Game("Sekiro: Shadows Die Twice", "From Software", 59.99);
		Game game5 = new Game("Shin Megami Tensei 3: Nocturne", "Atlus", 29.99);
		Game game6 = new Game("Super Smash Bros. Ultimate", "Nintendo", 79.99);
		Game game7 = new Game("Bloodborne", "From Software", 39.99);
		Game game8 = new Game("Griftlands", "Devolver Digital", 29.99);
		Game game9 = new Game("The Legend of Zelda: Breath of the Wild", "Nintendo", 79.99);
		Game game10 = new Game("Demon's Souls", "From Software", 39.99);
		
		gameRepo.save(game1);
		gameRepo.save(game2);
		gameRepo.save(game3);
		gameRepo.save(game4);
		gameRepo.save(game5);
		gameRepo.save(game6);
		gameRepo.save(game7);
		gameRepo.save(game8);
		gameRepo.save(game9);
		gameRepo.save(game10);
	}

}
