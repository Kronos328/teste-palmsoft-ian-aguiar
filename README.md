# Game CRUD
O presente projeto consiste em um CRUD de produtos utilizando o framework SpringBoot. Os produtos em questão são games, cada um com um nome, nome de desenvolvedora, preço e datas de criação e edição.

Com isso, é possível fazer as operações padrões de um CRUD: Criar novos jogos, ver e atualizar as informações de um jogo específico, deletar um jogo do banco.

O sistema possui validação dos campos digitados, paginação e utilza também o H2 como solução de banco de dados salvo em disco.

## Instalação
Antes de começar, certifique-se que tem o Open JDK 11 instalado.

Caso esteja usando Linux como seu SO, você pode baixá-lo utilizando o gerenciador de pacotes da sua distribuição.

No caso do windows, é possível baixar uma build feita pela microsoft do [OpenJDK 11](https://docs.microsoft.com/en-us/java/openjdk/download#openjdk-11)

Após a instalação do open JDK, baixe e descompacte esse projeto em algum lugar.

Então, vá para o diretório raiz do projeto (Aquele onde está o arquivo `pom.xml`) e abra um terminal nesse diretório (Use o bash caso esteja no Linux ou o git bash caso no windows).

Após abrir o terminal, será necessário instalar as dependências através do Maven:
  - Caso use Windows, digite: `./mvnw.cmd clean install`
  - Caso use Linux, digite: `./mvnw clean install`

Aguarde então até a execução completa do comando.

## Variável de ambiente

O projeto utiliza apenas uma variável de ambiente num arquivo `.env`, que pode ser vista no arquivo `.env.example`

É necessário criar esse arquivo `.env`, com o `.env.example` podendo ser usado de molde.

Caso a variável de ambiente em questão esteja definida como `True`, na inicialização inicial do banco (e em todas as vezes que o banco de dados se encontrar vazio), o sistma irá popular o DB com entratas de teste, para fins de convêniência na hora dos testes.

Caso não queira que esse comportamento ocorra, defina a variável como `False`.

Independente de tudo, é necessário que o arquivo `.env` exista, especificamente na raiz do projeto, ou seja, no mesmo diretório que o .`.env.example`

## Inicialização

Para iniciar o sistema, basta abrir um terminal no mesmo diretório aberto na hora da instalação e rodar:
  - Caso use Windows: `./mvnw.cmd spring-boot:run`
  - Caso use Linux: `./mvnw spring-boot:run`

Então, para acessá-lo, abra um navegador e vá até o endereço: `localhost:8080`
